const config = {
    port: 8090
}


const logger = require('./logger');
logger.setLogLevel(logger.LogLevel.INFO);

const express = require('express');
const cors = require('cors');
const http = require('http');

const app = express();

/**
 * Logs all the calls in the log level is INFO
 */
app.use(function (req, res, next) {
    logger.info(req.method + ' ' + req.url);
    next();
});

// Indicates if an OPTIONS call has happened
let optionsCallHappened = false;

/**
 * Set the CORS configuration based on the query parameters allowOrigin, allowMethods, allowHeaders, allowCredentials.
 * The callback function that will apply this config and handle the CORS response is then called with
 * this CORS configuration.
 * Note that the CORS response is composed of the OPTIONS call response (if happening) and the proper
 * CORS headers settings.
 * 
 * @param {*} req the actual HTTP request
 * @param {*} callback the callback function that will set the CORS config and let the process go
 */
function corsOptionsDelegate(req, callback) {
    if (req.method === 'OPTIONS') {
        optionsCallHappened = true;
        // if the next call (GET, POST, PUT, ...) doesn't happen right away (500ms), then we reset it to false
        setTimeout(function () {
            if (optionsCallHappened === true) {
                optionsCallHappened = false;
                logger.debug('Flag [optionsCallHappened] reset to false because no call happened after this OPTIONS call');
            }
        }, 500);
    }

    const corsOptions = {
        origin: 'http://localhost:4222', // Set HTTP origin allowed
        methods: 'GET,POST,PATCH,POST,DELETE', // Set HTTP methods allowed: GET,PUT,PATCH,POST,DELETE
        allowedHeaders: null, // Set headers allowed (null will allow them all)
        credentials: true // This allows HTTP Request with credentials
    };
    if (req.query) {
        corsOptions['origin'] = req.query.allowOrigin === 'true';
        logger.debug(' >> corsOptions[\'origin\'] = ' + corsOptions['origin']);
        corsOptions['methods'] = req.query.allowMethods === 'true' ? 'GET,POST,PATCH,POST,DELETE' : '';
        logger.debug(' >> corsOptions[\'methods\'] = ' + corsOptions['methods']);
        // corsOptions['allowedHeaders'] = Object.keys(req.headers).join(',').concat(',' + req.get('Access-Control-Request-Headers'))
        corsOptions['allowedHeaders'] = req.query.allowHeaders === 'true' ? null : 'none';
        logger.debug(' >> corsOptions[\'allowedHeaders\'] = ' + corsOptions['allowedHeaders']);
        corsOptions['credentials'] = req.query.allowCredentials === 'true';
        logger.debug(' >> corsOptions[\'credentials\'] = ' + corsOptions['credentials']);
    }
    callback(null, corsOptions)
}

/**
 * Handle all the calls toward the endpoint [/api/test] regardless of the HTTP method
 */
app.all('/api/test', cors(corsOptionsDelegate), function (req, res) {
    const result = {
        foo: 'bar-' + Math.random() * 1000000,
        optionsCallHappened: optionsCallHappened
    };
    res.status(200).json(result);
    optionsCallHappened = false;
});

// Start server
http.createServer(app).listen(config.port);
logger.info('API Server running (port: ' + config.port + ')');