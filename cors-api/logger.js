'use strict';

const LogLevel = {
    DEBUG: 2,
    INFO: 3,
    WARN: 4,
    ERROR: 5
};

let LOG_LEVEL = LogLevel.DEBUG;

function getLogLevelAsString(level) {
    return Object.keys(LogLevel).find(key => LogLevel[key] === level);
}

function setLogLevel(level) {
    if (getLogLevelAsString(level)) {
        LOG_LEVEL = level;
    } else {
        console.warn('Incorrect log level \'' + level + '\' provided, will be set as \'INFO\' by default. Log level must be one of: ' + Object.keys(LogLevel));
        LOG_LEVEL = LogLevel.INFO;
    }
    debug('Log level set as ' + getLogLevelAsString(LOG_LEVEL));
}

function debug(toPrint) {
    if (LOG_LEVEL <= LogLevel.DEBUG) {
        console.log(toPrint);
    }
}

function info(toPrint) {
    if (LOG_LEVEL <= LogLevel.INFO) {
        console.log(toPrint);
    }
}

function warn(toPrint) {
    if (LOG_LEVEL <= LogLevel.WARN) {
        console.warn(toPrint);
    }
}

function error(toPrint) {
    if (LOG_LEVEL <= LogLevel.ERROR) {
        console.error(toPrint);
    }
}

module.exports = {
    LogLevel: LogLevel,
    setLogLevel: setLogLevel,
    debug: debug,
    info: info,
    warn: warn,
    error: error
}