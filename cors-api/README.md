# Capco CORS Tester

This project's objective is to show how the Cross-Origin requests should be properly implemented.
It is very highly recommended to read some documentation before using this tool, just to have some notions before starting. We recommend [this page](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).  

The project is divided in two parts:
- The Angular app (cors-ui)
- The Rest API app (cors-api)

## Prerequisites

Node.js and NPM must be installed.
The versions used to create and run this project are Node.js 10.8.0 and NPM 6.2.0.

## How to run the Angular app?

In the folder `./cors-ui`:

1. Run command `npm install` to install the NPM dependencies
2. Run command `npm start` to run the Angular app

> This command will build and serve the Angular app, and it will use the Angular Proxy for all the API calls targeting `http://localhost:4222/api/*` endpoints

3. Navigate to `http://localhost:4222/`

> The app will automatically reload if you change any of the source files.

## How to run the Rest API app

In the folder `./cors-api`:

1. Run command `npm install` to install the NPM dependencies
2. Run command `npm start` to run the Rest API app
