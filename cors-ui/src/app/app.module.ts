import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MatCheckboxModule, MatTooltipModule, MatExpansionModule, MatButtonModule, MatDividerModule, MatRadioModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { DescriptionComponent } from './components/description/description.component';
import { ApiRequestComponent } from './components/api-request/api-request.component';

@NgModule({
  declarations: [
    AppComponent,
    DescriptionComponent,
    ApiRequestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonModule,
    MatDividerModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
