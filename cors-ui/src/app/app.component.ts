import { Component, ViewEncapsulation } from '@angular/core';
import { ApiService } from './services/api.service';
import { ApiCorsConfig, XhrCorsConfig, HeadersCorsConfig, CorsSample, ResponseCorsConfig, CorsMethodEnum } from './services/cors-sample.model';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { constants } from './app.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  mapApiConfig: { [key: string]: CorsSample } = {};
  environment = environment;
  isDescriptionPanelDisplayed = true;

  constructor(private api: ApiService) {
    this.mapApiConfig[CorsMethodEnum.GET] = new CorsSample(new ApiCorsConfig(
      new XhrCorsConfig(environment.apiOrigin, '/api/test', true),
      new HeadersCorsConfig('application/json', true, true, true),
      new ResponseCorsConfig(true, true, true, true)));
    this.mapApiConfig[CorsMethodEnum.POST] = new CorsSample(new ApiCorsConfig(
      new XhrCorsConfig(environment.apiOrigin, '/api/test', true),
      new HeadersCorsConfig('application/json', true, true, true),
      new ResponseCorsConfig(true, true, true, true)));
    this.mapApiConfig[CorsMethodEnum.PUT] = new CorsSample(new ApiCorsConfig(
      new XhrCorsConfig(environment.apiOrigin, '/api/test', true),
      new HeadersCorsConfig('application/json', true, true, true),
      new ResponseCorsConfig(true, true, true, true)));

    try {
      this.isDescriptionPanelDisplayed = localStorage.getItem(constants.sessionStorage.showDescriptionPanel) === 'true';
    } catch(e) {
      console.warn('Can\'t read from the session storage')
    }
  }

  showDescriptionPanel() {
    try {
      localStorage.setItem(constants.sessionStorage.showDescriptionPanel, 'true');
    } catch(e) {
      console.warn('Can\'t read from the session storage')
    }
  }
  hideDescriptionPanel() {
    try {
      localStorage.setItem(constants.sessionStorage.showDescriptionPanel, 'false');
    } catch(e) {
      console.warn('Can\'t read from the session storage')
    }
  }

  sendRequest(method) {
    if (method && this.mapApiConfig[method]) {
      this.mapApiConfig[method].response = null;
      this.mapApiConfig[method].error = null;
      this.api.callApiEndpoint(method, this.mapApiConfig[method].requestConfig).subscribe(
        (result) => {
          this.mapApiConfig[method].response = result;
        },
        (err: HttpErrorResponse) => {
          this.mapApiConfig[method].error = err.message;
        });
    }
  }

  listMethods() {
    return Object.keys(this.mapApiConfig);
  }

  isResultReady(method: string) {
    return method && this.mapApiConfig[method].response;
  }

  formatResult(method: string) {
    if (this.isResultReady(method)) {
      return JSON.stringify(this.mapApiConfig[method].response, null, 2);
    }
    return null;
  }

  getTitleForRequest(method: string) {
    return method + ' ' + this.mapApiConfig[method].requestConfig.xhr.origin + this.mapApiConfig[method].requestConfig.xhr.endpoint;
  }
}
