export enum CorsMethodEnum {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT'
}


export class CorsSample {
  requestConfig: ApiCorsConfig;
  response: any;
  error: any;

  constructor(requestConfig: ApiCorsConfig) {
    this.requestConfig = requestConfig;
  }
}

export class ApiCorsConfig {
  xhr: XhrCorsConfig;
  headers: HeadersCorsConfig;
  response: ResponseCorsConfig;

  constructor(xhr: XhrCorsConfig, headers: HeadersCorsConfig, response: ResponseCorsConfig) {
    this.xhr = xhr;
    this.headers = headers;
    this.response = response;
  }
}

export class XhrCorsConfig {
  origin: string;
  endpoint: string;
  withCredentials: boolean;

  constructor(origin: string, endpoint: string, withCredentials: boolean) {
    this.origin = origin;
    this.endpoint = endpoint;
    this.withCredentials = withCredentials;
  }
}

/**
 * Configuration of the CORS headers in the request
 */
export class HeadersCorsConfig {
  contentType: string;
  allowOrigin: boolean;
  allowCredentials: boolean;
  requestHeaders: boolean;

  constructor(contentType: string, allowOrigin: boolean, allowCredentials: boolean, requestHeaders: boolean) {
    this.contentType = contentType;
    this.allowOrigin = allowOrigin;
    this.allowCredentials = allowCredentials;
    this.requestHeaders = requestHeaders;
  }
}

/**
 * Configuration of the CORS response coming from the backend
 */
export class ResponseCorsConfig {
  allowOrigin: boolean; // In the backend, this will allow specific origin if true
  allowMethods: boolean; // In the backend, this will allow specific methods if true
  allowHeaders: boolean; // In the backend, this will allow specific headers if true
  allowCredentials: boolean; // In the backend, this will allow request with credentials if true

  /**
   * Create instance of a Response CORS Config
   * @param {boolean} allowOrigin will set the access-control-allow-origin properly in the backend
   * @param {boolean} allowMethods will set the access-control-allow-methods properly in the backend
   * @param {boolean} allowHeaders will set the access-control-allow-headers properly in the backend
   * @param {boolean} allowCredentials will set the access-control-allow-credentials properly in the backend
   */
  constructor(allowOrigin: boolean, allowMethods: boolean, allowHeaders: boolean,allowCredentials: boolean) {
    this.allowOrigin = allowOrigin;
    this.allowMethods = allowMethods;
    this.allowHeaders = allowHeaders;
    this.allowCredentials = allowCredentials;
  }
}
