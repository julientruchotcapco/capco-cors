import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiCorsConfig, CorsMethodEnum, HeadersCorsConfig, XhrCorsConfig } from './cors-sample.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private getHeaders(config: HeadersCorsConfig): HttpHeaders {
    const headers = new HttpHeaders({
      'Accept': config.contentType,
      'Content-Type': config.contentType
    });
    if (config.allowOrigin === true) {
      headers.append('Access-Control-Allow-Origin', window.location.origin);
    }
    if (config.allowCredentials === true) {
      headers.append('Access-Control-Allow-Credentials', 'true');
    }
    if (config.requestHeaders) {
      headers.append('Access-Control-Request-Headers', headers.keys().join(','));
    }
    return headers;
  }

  /**
   * Builds the URL based on:
   * - the XHR origin and endpoint targeted
   * - the response headers configuration
   * @param {ApiCorsConfig} config the configuration to use to build the URL
   * @return {string} the built URL
   */
  private buildUrl(config: ApiCorsConfig): string {
    let url: string = config.xhr.origin + config.xhr.endpoint;
    url += '?' + Object.keys(config.response).map(key => key + '=' + config.response[key]).join('&');
    return url;
  }

  callApiEndpoint(method: CorsMethodEnum, config: ApiCorsConfig): Observable<any> {
    switch(method) {
      case CorsMethodEnum.GET:
        return this.http.get(this.buildUrl(config), {
          observe: 'body', // Set as 'response' to always get the actual HTTP response (and not only the body)
          headers: this.getHeaders(config.headers),
          withCredentials: config.xhr.withCredentials
        });
      case CorsMethodEnum.POST:
        return this.http.post(this.buildUrl(config), {}, {
          observe: 'body', // Set as 'response' to always get the actual HTTP response (and not only the body)
          headers: this.getHeaders(config.headers),
          withCredentials: config.xhr.withCredentials
        });
      case CorsMethodEnum.PUT:
        return this.http.put(this.buildUrl(config), {}, {
          observe: 'body', // Set as 'response' to always get the actual HTTP response (and not only the body)
          headers: this.getHeaders(config.headers),
          withCredentials: config.xhr.withCredentials
        });
    }
  }

  // postThisTest(url: string, body: any): Observable<any> {
  //   return this.http.post(url, body, {
  //     reportProgress: true,
  //     observe: 'response',
  //     headers: this.getHeaders(),
  //     responseType: 'json',
  //     withCredentials: true
  //   });
  // }
}
