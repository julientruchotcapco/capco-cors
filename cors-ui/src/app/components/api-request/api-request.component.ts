import { Component, Input } from '@angular/core';
import { environment } from '../../../environments/environment';
import { CorsSample } from '../../services/cors-sample.model';

@Component({
  selector: 'app-api-request',
  templateUrl: './api-request.component.html',
  styleUrls: ['./api-request.component.css']
})
export class ApiRequestComponent {

  @Input() mapApiConfig: CorsSample;

  environment = environment;

  constructor() { }

}
