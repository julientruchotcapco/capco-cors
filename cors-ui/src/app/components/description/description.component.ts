import { Component } from '@angular/core';
import { environment } from '../../../environments/environment';
import { constants } from '../../app.constants';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent {

  environment = environment;
  isDescriptionPanelDisplayed = true;

  constructor() {
    try {
      this.isDescriptionPanelDisplayed = localStorage.getItem(constants.sessionStorage.showDescriptionPanel) === 'true';
    } catch(e) {
      console.warn('Can\'t read from the session storage')
    }
  }

  showDescriptionPanel() {
    try {
      localStorage.setItem(constants.sessionStorage.showDescriptionPanel, 'true');
    } catch(e) {
      console.warn('Can\'t read from the session storage')
    }
  }
  hideDescriptionPanel() {
    try {
      localStorage.setItem(constants.sessionStorage.showDescriptionPanel, 'false');
    } catch(e) {
      console.warn('Can\'t read from the session storage')
    }
  }

}
