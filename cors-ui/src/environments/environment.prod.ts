export const environment = {
  production: true,
  apiOrigin: 'http://localhost:8090',
  localOrigin: window.location.origin
};
